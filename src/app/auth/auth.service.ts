import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

import { User } from './user.model';
import { AuthData } from './auth-data.model';
import { TrainingService } from '../training/training.service';
import { UIService } from '../shared/ui.service';

@Injectable()
export class AuthService {
  authChange = new Subject<boolean>();
  private isAuthenticated = false;
  private user: User = null;

  constructor(
    private router: Router,
    private http: HttpClient,
    private trainingService: TrainingService,
    private uiService: UIService
  ) {}

  initAuthListener() {
    /// beremo user from token?
    /// subscribe
    /// inject it to AppComponent onInit
    /// ex this.afAuth.authState.subscribe(user => {
    //   if (user) {
    //     this.isAuthenticated = true;
    //     this.authChange.next(true);
    //     this.router.navigate(['/training']);
    //   } else {
    //     this.trainingService.cancelSubscriptions();
    //     this.authChange.next(false);
    //     this.router.navigate(['/login']);
    //     this.isAuthenticated = false;
    //   }
    // })
  }

  registerUser(authData: AuthData) {
    this.uiService.loadingStateChanged.next(true);
    const user = {
      email: authData.email,
      password: authData.password,
      userId: Math.round(Math.random() * 1000).toString()
    };
    console.log('registerUser user', user);
    this.http.post('/api/auth/signup', user)
      .subscribe(
        result => {
          this.uiService.loadingStateChanged.next(false);
          console.log('registerUser result:', result);
          this.authSuccessfully();
        },
        error => {
          this.uiService.loadingStateChanged.next(false);
          this.uiService.showSnackbar(error.message, null, 3000);
        }
      );
  }

  login(authData: AuthData) {
    this.uiService.loadingStateChanged.next(true);
    const user = {
      email: authData.email,
      password: authData.password,
      userId: Math.round(Math.random() * 10000).toString()
    };
    this.user = user;
    this.http.post('/api/auth/login', user)
    .subscribe(
      result => {
        this.uiService.loadingStateChanged.next(false);
        console.log('login result:', result);
        this.authSuccessfully();
      },
      error => {
        this.uiService.loadingStateChanged.next(false);
        this.uiService.showSnackbar(error.message, null, 3000);
      }
    );
  }

  logout() {
    this.user = null; // change to token...
    this.trainingService.cancelSubscriptions();
    this.authChange.next(false);
    this.isAuthenticated = false;
    this.router.navigate(['/login']);
  }

  // getUser(): User {
  //   return { ...this.user };
  // }

  isAuth() {
    return this.isAuthenticated;
    // return this.user !== null;
  }

  private authSuccessfully() {
    this.isAuthenticated = true;
    this.authChange.next(true);
    this.router.navigate(['/training']);
  }
}
