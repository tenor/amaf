export interface Exercise {
  id?: string;
  _id?: string;
  exercise_id?: string;
  name: string;
  duration: number;
  calories: number;
  date?: Date;
  state?: 'completed'| 'canceled' | null;
}
