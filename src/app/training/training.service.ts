import { Exercise } from './exercise.model';
import { Subject, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UIService } from '../shared/ui.service';

@Injectable()
export class TrainingService {
  exerciseChanged = new Subject<Exercise>();
  exercisesChanged = new Subject<Exercise[]>();
  finishedExercisesChanged = new Subject<Exercise[]>();
  private availableExercises: Exercise[] = [];
  // private availableExercises: Exercise[] = [
  //   {id: 'crunches', name: 'Crunches', duration: 30, calories: 8},
  //   {id: 'touch-toes', name: 'Touch Toes', duration: 180, calories: 15},
  //   {id: 'side-lunges', name: 'Side Lunges', duration: 120, calories: 18},
  //   {id: 'burpees', name: 'Burpees', duration: 60, calories: 8}
  // ];
  private runningExercise: Exercise;
  private finishedExercises: Exercise[] = [];
  private subscriptions: Subscription[] = [];

  constructor(private http: HttpClient, private uiService: UIService) { }

  getAvailableExercisesObs()/*: Observable<Exercise[]>*/ {
    this.uiService.loadingStateChanged.next(true);
    const getAvailables$ = this.http.get<Exercise[]>('/api/trainings').pipe(
      map(docs => {
        // throw(new Error());
        return docs.map(doc => {
          return {
            id: doc._id,
            name: doc.name,
            calories: doc.calories,
            duration: doc.duration
          };
        });
      })
    )
    .subscribe((exercises: Exercise[]) => {
      this.uiService.loadingStateChanged.next(false);
      this.availableExercises = exercises;
      this.exercisesChanged.next([ ...this.availableExercises ]);
    }, error => {
      this.uiService.loadingStateChanged.next(false);
      this.uiService.showSnackbar('Fetching Exercises failed, please try again later...', null, 3000);
      this.exercisesChanged.next(null);
    });
    this.subscriptions.push(getAvailables$);
  }

  startExercise(selectedId: string) {
    this.http.put(`/api/trainings/${selectedId}`, {lastSelected: new Date()})
      .subscribe(console.log);
    console.log('startExercise id', selectedId, this.availableExercises);
    this.runningExercise = this.availableExercises.find(element =>
      element.id === selectedId
    );
    this.exerciseChanged.next({...this.runningExercise});
  }

  completeExercise() {
    // this.exercises.push({
    //   ...this.runningExercise,
    //   date: new Date(),
    //   state: 'completed'
    // });
    this.addDataToDatabase({
      ...this.runningExercise,
      date: new Date(),
      state: 'completed'
    });
    this.runningExercise = null;
    this.exerciseChanged.next(null);
  }

  cancelExercise(progress: number) {
    // this.exercises.push({
    //   ...this.runningExercise,
    //   duration: this.runningExercise.duration * (progress / 100),
    //   calories: this.runningExercise.calories * (progress / 100),
    //   date: new Date(),
    //   state: 'canceled'
    // });
    this.addDataToDatabase({
      ...this.runningExercise,
      duration: this.runningExercise.duration * (progress / 100),
      calories: this.runningExercise.calories * (progress / 100),
      date: new Date(),
      state: 'canceled'
    });
    this.runningExercise = null;
    this.exerciseChanged.next(null);
  }

  getRunningExercise() {
    return { ...this.runningExercise };
  }

  fetchCompletedOrCancelledExercises() {
    const fetchFinished$ = this.http.get('/api/exercises/finished')
      .subscribe((finished: Exercise[]) => {
        this.finishedExercises = finished;
        this.finishedExercisesChanged.next([...this.finishedExercises]);
      });
    this.subscriptions.push(fetchFinished$);
  }

  cancelSubscriptions() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  private addDataToDatabase(exercise: Exercise) {
    console.log('addDataToDatabase exercise:', exercise);
    this.http.post('/api/exercises/finished', exercise)
      .subscribe(console.log);
  }
}
